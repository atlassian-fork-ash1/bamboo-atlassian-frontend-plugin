package com.atlassian.bamboo.atlassianfrontend;

import com.atlassian.bamboo.author.Author;
import com.atlassian.bamboo.builder.LifeCycleState;
import com.atlassian.bamboo.commit.Commit;
import com.atlassian.bamboo.notification.buildhung.BuildHungNotification;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.project.Project;
import com.atlassian.bamboo.repository.RepositoryData;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.resultsummary.vcs.RepositoryChangeset;
import com.atlassian.bamboo.variable.CustomVariableContext;
import com.google.common.collect.ImmutableList;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class AtlassianFrontendNotificationTransportTest
{
    private final String WEBHOOK_URL = "alamakotaakotmaapitoken";
    private final String PRODUCT = "CONF";
    private final Date commit1Date = new Date(2000 - 1900, Calendar.JANUARY, 21);
    private final Date commit2Date = new Date(2010 - 1900, Calendar.FEBRUARY, 21);

    @Mock
    private Project project;

    @Mock
    private RepositoryChangeset repositoryChangeset;

    @Mock
    private ResultsSummary resultSummary;

    @Mock
    private PlanResultKey planResultKey;

    @Mock
    private PlanKey planKey;

    @Mock
    private CustomVariableContext customVariableContext;

    @Mock
    private Commit commit1;

    @Mock
    private Commit commit2;

    @Mock
    private Author author1;

    @Mock
    private Author author2;

    @Test
    public void testCorrectUrlsAreHit()
    {

        when(commit1.getComment()).thenReturn("Comment 1");
        when(commit1.getChangeSetId()).thenReturn("asdfg");
        when(author1.getEmail()).thenReturn("bla@bar.com");
        when(commit1.getAuthor()).thenReturn(author1);
        when(commit1.getDate()).thenReturn(commit1Date);

        when(commit2.getComment()).thenReturn("Comment 1");
        when(commit2.getChangeSetId()).thenReturn("def123");
        when(author2.getEmail()).thenReturn("foo@bar.com");
        when(commit2.getAuthor()).thenReturn(author2);
        when(commit2.getDate()).thenReturn(commit2Date);

        Set<Commit> commitList =  new HashSet<Commit>();

        commitList.add(commit1);
        commitList.add(commit2);

        when(planResultKey.getKey()).thenReturn("DEST-TEST-0-100");
        when(planKey.getKey()).thenReturn("DEST-TEST-0");
        when(resultSummary.isSuccessful()).thenReturn(true);
        when(resultSummary.getLifeCycleState()).thenReturn(LifeCycleState.FINISHED);
        when(resultSummary.getBuildTime()).thenReturn("1");
        when(resultSummary.getPlanResultKey()).thenReturn(planResultKey);
        when(resultSummary.getPlanKey()).thenReturn(planKey);
        when(resultSummary.getShortReasonSummary()).thenReturn("A short summary");
        when(resultSummary.getReasonSummary()).thenReturn("A longer summary");

        when(repositoryChangeset.getChangesetId()).thenReturn("123");
        when(repositoryChangeset.getCommits()).thenReturn(commitList);

        when(resultSummary.getRepositoryChangesets()).thenReturn(Collections.singletonList(repositoryChangeset));
        when(customVariableContext.substituteString("alamakotaakotmaapitoken")).thenReturn("alamakotaakotmaapitoken");


        final PlanResultKey planResultKey = PlanKeys.getPlanResultKey("BAM-MAIN", 3);

        BuildHungNotification notification = new BuildHungNotification()
        {
            public String getHtmlImContent()
            {
                return "IM Content";
            }

        };

        AtlassianFrontendNotificationTransport hnt = new AtlassianFrontendNotificationTransport(WEBHOOK_URL, PRODUCT,resultSummary, customVariableContext);
        MockHttpClient httpClient = new MockHttpClient();
        //dirty reflections trick to inject mock HttpClient
        try
        {
            Field field = AtlassianFrontendNotificationTransport.class.getDeclaredField("client");
            field.setAccessible(true);
            field.set(hnt, httpClient);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            fail(e.getMessage());
        }

        hnt.sendNotification(notification);
        assertTrue(httpClient.isCalled());
    }

    public class MockHttpClient extends CloseableHttpClient {
        boolean isCalled = false;

        public boolean isCalled() {
            return this.isCalled;
        }

        @Override
        protected CloseableHttpResponse doExecute(HttpHost target, HttpRequest request, HttpContext context) throws IOException, ClientProtocolException {
            isCalled = true;
            assertTrue(request instanceof HttpPost);
            HttpPost postMethod = (HttpPost) request;
            assertEquals(WEBHOOK_URL, request.getRequestLine().getUri());

            assert(postMethod.getEntity() instanceof StringEntity);

            StringEntity entity = (StringEntity) postMethod.getEntity();
            JSONTokener tokener = new JSONTokener(IOUtils.toString(entity.getContent(), StandardCharsets.UTF_8.name()));

            JSONObject json = null;
            try {
                json = new JSONObject(tokener);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                assertEquals("true", json.getString("isSuccessful"));
                assertEquals(LifeCycleState.FINISHED.toString(), json.getString("lifeCycleState"));
                assertEquals("1", json.getString("buildTime"));
                assertEquals("DEST-TEST-0-100", json.getString("planResultKey"));
                assertEquals("DEST-TEST-0", json.getString("planKey"));
                assertEquals("A short summary", json.getString("shortReasonSummary"));
                assertEquals("A longer summary", json.getString("reasonSummary"));
                assertEquals(PRODUCT, json.getString("product"));
                JSONObject changesets = json.getJSONArray("changesets").getJSONObject(0);
                assertEquals("123", changesets.getString("id"));
                JSONArray commits = changesets.getJSONArray("commits");
                JSONObject commit1Json = commits.getJSONObject(0);

                assertEquals("Comment 1", commit1Json.getString("message"));
                assertEquals("asdfg", commit1Json.getString("changeSetId"));
                assertEquals("bla@bar.com", commit1Json.getString("authorEmail"));

            } catch (JSONException e) {
                assertTrue(false);
            }

            return null;
        }

        public void close() throws IOException {

        }

        public HttpParams getParams() {
            return null;
        }

        public ClientConnectionManager getConnectionManager() {
            return null;
        }
    }
}
