Atlassian Frontend Webhook notifications for Bamboo
==============================

This plugin sends [Bamboo](https://www.atlassian.com/software/bamboo) notifications to the Atlassian Frontend Build status communicator

The service we use for communicating build statuses has not been open sourced (yet), so this plugin is not really going to be of much
service to anyone.

Notifications Supported
-----------------------

-	Build successful
-	Build failed
-	Build commented
-	Job hung
-	Job queue timeout

Setup
-----

1.	Go to the *Notifications* tab of the *Configure Plan* screen.
2.	Choose a *Recipient Type* of *Atlassian Frontend*
3.	Configure your *Atlassian Frontend Build status communicator afWebhookUrl* : Talk to AFP if you need to know it. 
4.	You're done! Go and get building.

Compiling from source
---------------------

You first need to [Set up the Atlassian Plugin SDK](https://developer.atlassian.com/docs/getting-started/set-up-the-atlassian-plugin-sdk-and-build-a-project). Or you could just do a `brew tap atlassian/tap; brew install atlassian/tap/atlassian-plugin-sdk` on a mac is you use HomeBrew... At the project top level (where the pom.xml is) :

1.	Compile : `atlas-mvn compile`
2.	Run : `atlas-run`
3.	Debug : `atlas-debug`

Feedback? Questions?
--------------------

@M_de_Jongh

Many Thanks to @0x4d4d for building the [original bamboo slack plugin](https://bitbucket.org/mathieumarache/bamboo-slack-plugin/) which this repo is a fork of. 
Twitter: @0x4d4d or mathieu.marache@cstb.fr.
